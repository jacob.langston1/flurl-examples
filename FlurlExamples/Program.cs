﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FlurlExamples.Models;

namespace FlurlExamples
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var service = new ExampleService();
            var retryPosts = await service.GetPostsWithRetry();
            WritePosts(retryPosts);
            
            var policyPosts = await service.GetPostsWithPolicyStrategy();
            WritePosts(policyPosts);
        }

        private static void WritePosts(IEnumerable<Post> posts)
        {
            foreach (var post in posts)
            {
                Console.WriteLine(post.Title);
            }
            
            Console.ReadKey();
        }
    }
}