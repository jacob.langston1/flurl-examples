using System;
using Flurl;
using Flurl.Http;

namespace FlurlExamples
{
    public static class ExtensionExamples
    {
        public static IFlurlRequest AddAuthHeader(this IFlurlRequest req, string header)
        {
            req.Headers.Add("Authorization", header);

            return req;
        }

        public static IFlurlRequest AddAuthHeader(this Url url, string header) => new FlurlRequest(url).AddAuthHeader(header);
        public static IFlurlRequest AddAuthHeader(this Uri uri, string header) => new FlurlRequest(uri).AddAuthHeader(header);
        public static IFlurlRequest AddAuthHeader(this string url, string header) => new FlurlRequest(url).AddAuthHeader(header);
    }
}