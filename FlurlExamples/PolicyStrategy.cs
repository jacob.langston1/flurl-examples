using System;
using System.Collections.Generic;
using System.Net;
using Flurl.Http;
using Polly;
using Polly.CircuitBreaker;
using Polly.Retry;
using Polly.Wrap;

namespace FlurlExamples
{
/*
    https://stackoverflow.com/questions/42093593/execute-multiple-policies
    https://www.programmerall.com/article/30982015724/
    https://brunomj.medium.com/net-5-0-resilient-http-client-with-polly-and-flurl-b7de936fd70c
    https://www.twilio.com/blog/using-polly-circuit-breakers-resilient-net-web-service-consumers
*/
    public class PolicyStrategy
    {
        public AsyncPolicyWrap Get()
        {
            var retryPolicy = RetryPolicy();
            var circuitBreakerPolicy = CircuitBreakerPolicy();

            return retryPolicy.WrapAsync(circuitBreakerPolicy);
        }

        private bool IsTransientError(FlurlHttpException exception)
        {
            var httpStatusCodesWorthRetrying = new List<int>()
                    {
                        (int)HttpStatusCode.RequestTimeout, // 408
                        (int)HttpStatusCode.BadGateway, // 502
                        (int)HttpStatusCode.ServiceUnavailable, // 503
                        (int)HttpStatusCode.GatewayTimeout // 504
                    };

            return exception.StatusCode.HasValue && httpStatusCodesWorthRetrying.Contains(exception.StatusCode.Value);
        }

        /*
         * Circuit is open if 25% of requests fail in a 1 minute window, with a min of 7 requests in the window and
         * then the circuit is open for 30 seconds.
         */
        private AsyncCircuitBreakerPolicy CircuitBreakerPolicy()
        {
            var policy = Policy
                .Handle<FlurlHttpException>(IsTransientError)
                .AdvancedCircuitBreakerAsync(0.25, TimeSpan.FromMinutes(1), 
                    7, TimeSpan.FromSeconds(30), OnBreak,  OnReset, OnHalfOpen);

            return policy;
        }
        
        private AsyncRetryPolicy RetryPolicy()
        {
            var retryPolicy = Policy
                .Handle<FlurlHttpException>(IsTransientError)
                .WaitAndRetryAsync(3, retryAttempt =>
                {
                    var nextAttemptIn = TimeSpan.FromSeconds(Math.Pow(2, retryAttempt));
                    Console.WriteLine($"Retry attempt {retryAttempt} to make request. Next try on {nextAttemptIn.TotalSeconds} seconds.");
                    return nextAttemptIn;
                });

            return retryPolicy;
        }
        
        private void OnHalfOpen()
        {
            Console.WriteLine("Circuit in test mode, one request will be allowed.");
        }

        private void OnReset(Context obj)
        {
            Console.WriteLine("Circuit closed, requests flow normally.");
        }

        private void OnBreak(Exception arg1, CircuitState arg2, TimeSpan arg3, Context arg4)
        {
            Console.WriteLine("Circuit cut, requests will not flow.");
        }
    }
}