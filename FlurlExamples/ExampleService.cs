using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Flurl.Http;
using FlurlExamples.Exceptions;
using FlurlExamples.Models;
using Polly;
using Polly.Retry;

namespace FlurlExamples
{
    public class ExampleService
    {
        public async Task<IEnumerable<Post>> GetPosts()
        {
            try
            {
                return await "https://jsonplaceholder.typicode.com/posts".GetJsonAsync<IEnumerable<Post>>();
            }
            catch (FlurlHttpException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new InternalServerErrorException();
            }
        }

        public async Task<IEnumerable<Post>> GetPostsWithAuthHeader()
        {
            try
            {
                return await "https://jsonplaceholder.typicode.com/posts".AddAuthHeader("test").GetJsonAsync<IEnumerable<Post>>();
            }
            catch (FlurlHttpException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new InternalServerErrorException();
            }
        }

        public async Task<Post> GetPost(int id)
        {
            return await $"https://jsonplaceholder.typicode.com/posts/{id}".GetJsonAsync<Post>();
        }

        public async Task<Post> Create(Post post)
        {
            return await "https://jsonplaceholder.typicode.com/posts".PostJsonAsync(post).ReceiveJson<Post>();
        }

        public async Task<Post> Update(Post post)
        {
            return await $"https://jsonplaceholder.typicode.com/posts/{post.Id}".PutJsonAsync(post).ReceiveJson<Post>();
        }

        public async Task<IEnumerable<Post>> GetPostsWithRetry()
        {
            try
            {
                var policy = BuildRetryPolicy();

                var posts = await policy.ExecuteAndCaptureAsync(async () =>
                    await "https://jsonplaceholder.typicode.com/posts".GetJsonAsync<IEnumerable<Post>>());
                if (posts.Outcome != OutcomeType.Successful)
                {
                    Console.WriteLine("Supplier API is not available.");
                    throw new ServiceUnavailableException();
                }

                return posts.Result;
            }
            catch (FlurlHttpException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            catch (ServiceUnavailableException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new InternalServerErrorException();
            }
        }

        public async Task<IEnumerable<Post>> GetPostsWithPolicyStrategy()
        {
            try
            {
                var policy = new PolicyStrategy().Get();

                var posts = await policy.ExecuteAndCaptureAsync(async () =>
                    await "https://jsonplaceholder.typicode.com/posts".GetJsonAsync<IEnumerable<Post>>());
                if (posts.Outcome != OutcomeType.Successful)
                {
                    Console.WriteLine("Supplier API is not available.");
                    throw posts.FinalException;
                }

                return posts.Result;
            }
            catch (FlurlHttpException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            catch (ServiceUnavailableException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new InternalServerErrorException();
            }
        }

        private bool IsTransientError(FlurlHttpException exception)
        {
            var httpStatusCodesWorthRetrying = new List<int>()
            {
                (int)HttpStatusCode.RequestTimeout, // 408
                (int)HttpStatusCode.BadGateway, // 502
                (int)HttpStatusCode.ServiceUnavailable, // 503
                (int)HttpStatusCode.GatewayTimeout // 504
            };

            return exception.StatusCode.HasValue && httpStatusCodesWorthRetrying.Contains(exception.StatusCode.Value);
        }

        private AsyncRetryPolicy BuildRetryPolicy()
        {
            var retryPolicy = Policy
                .Handle<FlurlHttpException>(IsTransientError)
                .WaitAndRetryAsync(3, retryAttempt =>
                {
                    var nextAttemptIn = TimeSpan.FromSeconds(Math.Pow(2, retryAttempt));
                    Console.WriteLine($"Retry attempt {retryAttempt} to make request. Next try on {nextAttemptIn.TotalSeconds} seconds.");
                    return nextAttemptIn;
                });

            return retryPolicy;
        }
    }
}