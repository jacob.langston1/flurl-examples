using System.Collections.Generic;
using System.Net.Http;
using Flurl.Http;
using Flurl.Http.Testing;
using FlurlExamples.Exceptions;
using FlurlExamples.Models;
using Newtonsoft.Json;
using Xunit;

namespace FlurlExamples.UnitTests
{
    public class ExampleServiceTests
    {
        private readonly ExampleService _sut;

        public ExampleServiceTests()
        {
            _sut = new ExampleService();
        }

        /*
         * Call real API during test.
         */
        [Fact]
        public async void GetPostsReturnsAllUsingRealApi()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest
                    .ForCallsTo("https://jsonplaceholder.typicode.com/*")
                    .AllowRealHttp();
                // act
                var posts = await _sut.GetPosts();
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Get);
            }
        }

        [Fact]
        public async void GetPostsReturnsAllUsingFake()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                var body = new List<Post>()
                {
                    new Post()
                    {
                        Body = "I made a post!",
                        Id = 1,
                        UserId = 100,
                        Title = "My First Post"
                    }
                };
                httpTest.RespondWith(JsonConvert.SerializeObject(body), 200);

                // act
                var posts = await _sut.GetPosts();
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Get);
            }
        }

        [Fact]
        public async void GetPostsThrowsFlurlExceptions()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest.RespondWith(status: 401);
                // act
                var ex = await Assert.ThrowsAsync<FlurlHttpException>(async () => await _sut.GetPosts());
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Get);
            }
        }

        [Fact]
        public async void GetPostsWithAuthHeaderShouldHaveHeader()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest.RespondWith(status: 200);
                // act
                await _sut.GetPostsWithAuthHeader();
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Get)
                    .WithHeader("Authorization", "test");
            }
        }

        [Fact]
        public async void GetPostReturnsUsingRealApi()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest
                    .ForCallsTo("https://jsonplaceholder.typicode.com/*")
                    .AllowRealHttp();
                // act
                var posts = await _sut.GetPost(1);
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts/1")
                    .WithVerb(HttpMethod.Get);
            }
        }

        [Fact]
        public async void CreateReturnsUsingRealApi()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest
                    .ForCallsTo("https://jsonplaceholder.typicode.com/*")
                    .AllowRealHttp();
                // act
                var post = await _sut.Create(new Post()
                {
                    Body = "I made a post!",
                    UserId = 100,
                    Title = "My First Post"
                });
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Post);
            }
        }

        [Fact]
        public async void UpdateReturnsUsingRealApi()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest
                    .ForCallsTo("https://jsonplaceholder.typicode.com/*")
                    .AllowRealHttp();
                // act
                var post = await _sut.Update(new Post()
                {
                    Body = "I made a post!",
                    UserId = 100,
                    Title = "My First Post",
                    Id = 1
                });
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts/1")
                    .WithVerb(HttpMethod.Put);
            }
        }

        [Theory]
        [InlineData(408)]
        [InlineData(502)]
        [InlineData(503)]
        [InlineData(504)]
        public async void GetPostsWithRetryReturnsServiceUnavailable(int httpStatusCode)
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest.RespondWith(status: httpStatusCode);

                // act
                await Assert.ThrowsAsync<ServiceUnavailableException>(async () => await _sut.GetPostsWithRetry());
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Get);
            }
        }

        [Fact]
        public async void GetPostsWithRetryReturnsOk()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest.RespondWith(status: 200);

                // act
                var posts = await _sut.GetPostsWithRetry();
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Get);
            }
        }

        [Theory]
        [InlineData(408)]
        [InlineData(500)]
        [InlineData(502)]
        [InlineData(503)]
        [InlineData(504)]
        public async void GetPostsWithPolicyStrategyReturnsFlurlExceptions(int httpStatusCode)
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest.RespondWith(status: httpStatusCode);

                // act
                var ex = await Assert.ThrowsAsync<FlurlHttpException>(async () => await _sut.GetPostsWithPolicyStrategy());
                Assert.Equal(httpStatusCode, ex.StatusCode);
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Get);
            }
        }
        
        [Fact]
        public async void GetPostsWithPolicyStrategyReturnsOk()
        {
            using (var httpTest = new HttpTest())
            {
                // arrange
                httpTest.RespondWith(status: 200);

                // act
                var posts = await _sut.GetPostsWithPolicyStrategy();
                // assert
                httpTest.ShouldHaveCalled("https://jsonplaceholder.typicode.com/posts")
                    .WithVerb(HttpMethod.Get);
            }
        }
    }
}